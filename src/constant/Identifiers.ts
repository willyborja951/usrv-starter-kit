/**
 * Injection identifiers
 */

export type containerSymbol = {
  HelloService: symbol;
  HelloGateway: symbol;
};

const IDENTIFIERS: containerSymbol = {
  HelloGateway: Symbol.for("HelloGateway"),
  HelloService: Symbol.for("HelloService"),
};

export { IDENTIFIERS };
