/**
 *  Hello Handler
 */
import {
  ERROR_LAMBDA_MIDDLEWARE,
  IAPIGatewayEvent,
  IDENTIFIERS as ID,
  IHandler,
  INPUT_OUTPUT_LOGS,
  IRollbar,
  PARTIAL_RESPONSE_MIDDLEWARE,
  SETUP_MIDDLEWARE,
  SSM_MIDDLEWARE,
} from "@kushki/core";
import { Handler } from "aws-lambda";
import { IDENTIFIERS } from "constant/Identifiers";
import { CONTAINER } from "infrastructure/Container";
import * as middy from "middy";
import "reflect-metadata";
import { IHelloService } from "repository/IHelloService";
import * as Rollbar from "rollbar";
import "source-map-support/register";

const CORE: IHandler = CONTAINER.get<IHandler>(ID.Handler);
const ROLLBAR: Rollbar = CONTAINER.get<IRollbar>(ID.Rollbar).init();
const HANDLER: middy.Middy<IAPIGatewayEvent<string | object>, object> = middy<
  Handler<IAPIGatewayEvent<string | object>>
>(
  ROLLBAR.lambdaHandler(
    CORE.run<
      IHelloService, // Service Definition
      object // Service observable resolve type
    >(
      IDENTIFIERS.HelloService, // Service Symbol
      "getGoodbye", // Service Method
      CONTAINER,
      ROLLBAR
    )
  )
)
  // Middlewares (https://middy.js.org/)
  .use(SETUP_MIDDLEWARE(ROLLBAR))
  .use(INPUT_OUTPUT_LOGS(ROLLBAR))
  .use(ERROR_LAMBDA_MIDDLEWARE(ROLLBAR))
  .use(SSM_MIDDLEWARE(ROLLBAR))
  .use(PARTIAL_RESPONSE_MIDDLEWARE(ROLLBAR, "name,remainTime"));

export { HANDLER };
